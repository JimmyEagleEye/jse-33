package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectByIdSetStatusCommand extends AbstractProjectCommand {


    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-change-status-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Change project status by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsById(userId, id)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().changeProjectStatusById(userId, id, Status.getStatus(TerminalUtil.nextLine()));
    }


}

