package ru.korkmasov.tsc.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_TASK_VIEW_BY_ID = "task-view-by-id";

    String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";

}